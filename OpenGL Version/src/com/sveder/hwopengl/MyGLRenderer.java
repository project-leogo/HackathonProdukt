package com.sveder.hwopengl;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import android.content.Context;
import android.graphics.*;
import android.media.Image;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.opengl.GLUtils;
import android.util.Log;
import org.opencv.android.Utils;
import org.opencv.core.*;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

/**
 *  OpenGL Custom renderer used with GLSurfaceView 
 */
public class MyGLRenderer implements GLSurfaceView.Renderer {
	private static final int GL_TEXTURE_EXTERNAL_OES = 0x8D65;

   Context context;   // Application's context

   Square left;           // ( NEW )

   Square right;
   
   int[] texture = new int[1];
   MainActivity delegate;
   private SurfaceTexture surface;

   // Constructor with global application context
   public MyGLRenderer(MainActivity context) {
      this.delegate = context;
      left = new Square();         // ( NEW )
      right = new Square();         // ( NEW )

   }
   
   // Call back when the surface is first created or re-created
   @Override
   public void onSurfaceCreated(GL10 gl, EGLConfig config) {
      gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);  // Set color's clear-value to black
      gl.glClearDepthf(1.0f);            // Set depth's clear-value to farthest
      gl.glEnable(GL10.GL_DEPTH_TEST);   // Enables depth-buffer for hidden surface removal
      gl.glDepthFunc(GL10.GL_LEQUAL);    // The type of depth testing to do
      gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);  // nice perspective view
      gl.glShadeModel(GL10.GL_SMOOTH);   // Enable smooth shading of color
      gl.glDisable(GL10.GL_DITHER);      // Disable dithering for better performance



      createTexture(gl);

//      gl.glEnable(GL10.GL_TEXTURE_2D);  // Enable texture (NEW)
//      gl.glEnable(GL_TEXTURE_EXTERNAL_OES);

      //right.loadTexture(gl, delegate);
  
      // You OpenGL|ES initialization code here
      // ......
   }
   
   private void  createTexture(GL10 gl) {
	   
	   gl.glGenTextures(1,texture, 0);
	   delegate.startCamera(texture[0]);
	   
	   gl.glBindTexture(GL_TEXTURE_EXTERNAL_OES, texture[0]);
	   gl.glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
	   gl.glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
	   

//	   gl.glTexParameteri(GL_TEXTURE_EXTERNAL_OES, 
//	             GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);
}

// Call back after onSurfaceCreated() or whenever the window's size changes
   @Override
   public void onSurfaceChanged(GL10 gl, int width, int height) {
      if (height == 0) height = 1;   // To prevent divide by zero
      float aspect = (float)width / height;
   
      // Set the viewport (display area) to cover the entire window
      gl.glViewport(0, 0, width, height);
  
      // Setup perspective projection, with aspect ratio matches viewport
      gl.glMatrixMode(GL10.GL_PROJECTION); // Select projection matrix
      gl.glLoadIdentity();                 // Reset projection matrix
      // Use perspective projection
      GLU.gluPerspective(gl, 45, aspect, 0.1f, 100.f);
  
      gl.glMatrixMode(GL10.GL_MODELVIEW);  // Select model-view matrix
      gl.glLoadIdentity();                 // Reset
      

      // You OpenGL|ES display re-sizing code here
      // ......
   }


   // Call back to draw the current frame.
   @Override
   public void onDrawFrame(GL10 gl) {       
      // Clear color and depth buffers using clear-value set earlier
      gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
      gl.glLoadIdentity();


      Mat m = convertToMat(gl);
      m = transform(m);
      gl = convertToGl(m, gl);

      float[] mtx = new float[16];
      surface.updateTexImage();
      surface.getTransformMatrix(mtx);




      gl.glTranslatef(-1.25f, 0.0f, -4.0f); // Translate left and into the screen ( NEW )
      left.draw(gl);



      // Translate right, relative to the previous translation ( NEW )
      gl.glTranslatef(3.0f, 0.0f, 0.0f);
      right.draw(gl);                       // Draw quad ( NEW )
      
      
   }
   private Mat convertToMat(GL10 gl){

      int width = 1920;
      int height = 1080;

      Mat img =new Mat(height, width, CvType.CV_8UC4);

//use fast 4-byte alignment (default anyway) if possible
      gl.glPixelStorei(gl.GL_PACK_ALIGNMENT, (int) img.step1());

//set length of one complete row in destination data (doesn't need to equal img.cols)
      gl.glPixelStorei(1080, ((int) img.step1())/(int)img.elemSize());//TODO



      ByteBuffer raw_data = ByteBuffer.allocateDirect(height*width);


      gl.glReadPixels(0, 0, width, height, gl.GL_RGBA /*BGR*/, gl.GL_UNSIGNED_BYTE, raw_data); //TODO

      byte[] data = raw_data.array();
      byte[] datanew = new byte[data.length/4*3];

      for(int i = 0; i<width; i++){
         for(int j = 0; j<height; j++){
            datanew[(i+j*width)*3+0] = data[(i+j*width)*4+2];
            datanew[(i+j*width)*3+1] = data[(i+j*width)*4+1];
            datanew[(i+j*width)*3+2] = data[(i+j*width)*4+0];
         }

      }
      img.put(0, 0, raw_data.array());
      /*
      YuvImage yuv = new YuvImage(data, ImageFormat.YUY2, width, height, null);


      ByteArrayOutputStream out = new ByteArrayOutputStream();
      yuv.compressToJpeg(new android.graphics.Rect(0, 0, width, height), 50, out);

      byte[] bytes = out.toByteArray();
      final Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

    //  Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
     // bmp.copyPixelsFromBuffer(raw_data);
   //   Mat img= new Mat(bmp.getHeight(),bmp.getWidth(),CvType.CV_8UC3);
      Bitmap myBitmap32 = bmp.copy(Bitmap.Config.ARGB_8888, true);
      Utils.bitmapToMat(myBitmap32, img);

    //  Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2RGB,4);
*/
      return img;

   }



   cleanDetectFace detect = new cleanDetectFace();

   private Mat transform(Mat m){
      //TODO
     return detect.detectAndDisplay(m, delegate);
   }

   private GL10 convertToGl(Mat mat, GL10 gl) {
      // Generate a number for our textureID's unique handle
      gl.glGenTextures(1, texture, 0);

      // Bind to our texture handle
      gl.glBindTexture(GL10.GL_TEXTURE_2D, texture[0]);


      // Set texture interpolation methods for minification and magnification
      gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_TEXTURE_MIN_FILTER);
      gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_TEXTURE_MAG_FILTER);

      // Set texture clamping method
      gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_TEXTURE_WRAP_S);
      gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_TEXTURE_WRAP_T);

      // Set incoming texture format to:
      // GL_BGR       for CV_CAP_OPENNI_BGR_IMAGE,
      // GL_LUMINANCE for CV_CAP_OPENNI_DISPARITY_MAP,
      // Work out other mappings as required ( there's a list in comments in main() )
      int inputColourFormat = 1; //TODO BGR
      if (mat.channels() == 1) {
         inputColourFormat = gl.GL_LUMINANCE;
      }

      byte[] return_buff = new byte[(int) (mat.total() *
              mat.channels())];
      mat.get(0, 0, return_buff);

      ByteBuffer b = ByteBuffer.wrap(return_buff);

      // Create the texture
      gl.glTexImage2D(GL10.GL_TEXTURE_2D,     // Type of texture
              0,                 // Pyramid level (for mip-mapping) - 0 is the top level
              inputColourFormat,            // Internal colour format to convert to //TODO
              mat.cols(),          // Image width  i.e. 640 for Kinect in standard mode
              mat.rows(),          // Image height i.e. 480 for Kinect in standard mode
              0,                 // Border width in pixels (can either be 1 or 0)
              gl.GL_RGB, // Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
              GL10.GL_UNSIGNED_BYTE,  // Image data type
              b);        // The actual image data itself

      // If we're using mipmaps then generate them. Note: This requires OpenGL 3.0 or higher

      return gl;

   }

   public void setSurface(SurfaceTexture _surface)
   {
       surface = _surface;
   }
}