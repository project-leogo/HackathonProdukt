package com.sveder.hwopengl;


import android.app.Activity;
import android.content.Context;
import android.graphics.*;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.SurfaceHolder;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.*;
import org.opencv.core.Point;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class MainActivity extends Activity implements SurfaceTexture.OnFrameAvailableListener, Camera.PreviewCallback
{

    private Camera mCamera;

	private MyGLSurfaceView glSurfaceView;
    MyGLRenderer renderer;
	private SurfaceTexture surface;

	static{
		if (!OpenCVLoader.initDebug()) {
			Log.e("lala", "opencv is fürn arsch");
		}else{
			Log.e("lala", "vllt auch nicht");
		}
	}

	ImageView iv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
		 requestWindowFeature(Window.FEATURE_NO_TITLE);
	     getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
	                                WindowManager.LayoutParams.FLAG_FULLSCREEN);

	     getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	     
	     glSurfaceView = new MyGLSurfaceView(this);           // Allocate a GLSurfaceView
	     renderer = glSurfaceView.getRenderer();

	//	iv =new ImageView(this);

	    	     this.setContentView(glSurfaceView);                // This activity sets to GLSurfaceView

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void startCamera(int texture)
    {
        surface = new SurfaceTexture(texture);
        surface.setOnFrameAvailableListener(this);
        renderer.setSurface(surface);

        mCamera = Camera.open();

		try {
			mCamera.setPreviewTexture(surface);
		}catch(Exception bla){
			bla.printStackTrace();
		}
		//	mCamera.startPreview();


		// The camera preview was automatically stopped. Start it again.
		mCamera.setPreviewCallback(this);
		mCamera.startPreview();
    }

	@Override
	public void onFrameAvailable(SurfaceTexture arg0) {
    	Log.w("hi","MEOWWWWW");
		glSurfaceView.requestRender();
		
	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {


/*
		Camera.Parameters parameters = camera.getParameters();
		int width = parameters.getPreviewSize().width;
		int height = parameters.getPreviewSize().height;

		YuvImage yuv = new YuvImage(data, parameters.getPreviewFormat(), width, height, null);


		ByteArrayOutputStream out = new ByteArrayOutputStream();
		yuv.compressToJpeg(new Rect(0, 0, width, height), 50, out);

		byte[] bytes = out.toByteArray();
		final Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
/*
		MyActivity.this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				((ImageView) findViewById(R.id.loopback)).setImageBitmap(bitmap);
			}
		});*/

		//Bitmap bmp = BitmapFactory.decodeByteArray(data , 0, data.length);
/*		if(bmp == null){
			return;
		}

		Mat orig = new Mat(bmp.getHeight(),bmp.getWidth(), CvType.CV_8UC3);

		Utils.bitmapToMat(bmp, orig);

		Core.line(orig, new Point(0,0),new Point(300,300), new Scalar(100,100,100), 15);

		Bitmap copy = Bitmap.createBitmap(orig.cols(), orig.rows(), Bitmap.Config.ARGB_8888);

		Utils.matToBitmap(orig, copy);

		iv.setImageBitmap(copy);
/*
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);

		data = stream.toByteArray();

		camera.getParameters();*/

	}
}
